package tema5tp;


public class MonitoredData {
	private DateTime startTime;
	private DateTime endTime;
	private String activityLabel;
	//private List<MonitoredData> date;

	public MonitoredData(){
		
	}
	
	public MonitoredData(DateTime a,DateTime b,String as){
		this.setStartTime(a);
		this.setEndTime(b);
		this.setActivityLabel(as);
	}

	

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}





}

