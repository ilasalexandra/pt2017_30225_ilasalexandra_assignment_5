package tema5tp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Map.Entry;




public class Monitoring {
	
	private List<MonitoredData> date;
	private HashMap<String, Integer> actionMap; 
	private List<String> filteredActivities;
	
	
	public HashMap<String, Integer> getMap() {
		return this.actionMap;
	}
	public List<MonitoredData> getDate() {
		return date;
	}

	public void setDate(List<MonitoredData> date) {
		this.date = date;
	}

	
	public Monitoring(){
		this.setDate(new ArrayList<MonitoredData>());
		this.actionMap=new HashMap<String, Integer>();
		this.setFilteredActivities(new ArrayList<String>());

	}
	
	public List<String> readFile() throws IOException{

		 List<String> lines = Files.readAllLines(Paths.get("C:/Users/Alex/Desktop/Activities.txt"));
	       return lines;	
	}
	
	public DateTime createTimeData(String data)                      //convertirea unui string in tip data timp
	{
			DateTime d1=new DateTime();           
		
			StringTokenizer stok = new StringTokenizer(data, " ");
			String[]tokens = new String[stok.countTokens()];
			//System.out.println(tokens);
			for(int i=0; i<tokens.length; i++)
				{
				
				tokens[i] = stok.nextToken();
				//System.out.println(tokens[i]+"\n");
			
				}
			
			for(int i=0;i<tokens.length;i++)
			{
				String string= tokens[i];
				//System.out.println(string+"\n");
				if(i%2==0)
				{
					
					String aux1[]=string.split("-");
					d1.setYear(Integer.parseInt(aux1[0]));
					d1.setMonth(Integer.parseInt(aux1[1]));
					d1.setDay(Integer.parseInt(aux1[2]));
				
					//System.out.println(d1.getYear()+" "+d1.getMonth()+ " "+ d1.getDay()+"\n");
				}
				else
				{
					String aux1[]=string.split(":");
					d1.setHour(Integer.parseInt(aux1[0]));
					d1.setMinute(Integer.parseInt(aux1[1]));
					d1.setSecond(Integer.parseInt(aux1[2]));
					//System.out.println(d1.getHour()+" "+d1.getMinute()+" "+ d1.getSecond()+"\n");
				}
				
			}
				
			return d1;
		}
	
	public void alldata(List<String> lines){                          //citire din fisier convertita in monitored data
		List<MonitoredData> data= new ArrayList<MonitoredData>();
		String[] tokens;
		for(String s:lines)
		{
			StringTokenizer stok = new StringTokenizer(s, "		");
			tokens = new String[stok.countTokens()];
			//System.out.println(tokens);
			for(int i=0; i<tokens.length; i++)
				{
				tokens[i] = stok.nextToken();
				//System.out.println(tokens[i]+"\n");
			
				}
			for(int i=0;i<tokens.length;i++)
			{
				MonitoredData x=new MonitoredData();
				x.setStartTime(createTimeData(tokens[i]));
				x.setEndTime(createTimeData(tokens[i+1]));
				x.setActivityLabel(tokens[i+2]);
				i+=3;
				data.add(x);
				//System.out.println(x.getStartTime()+"\n"+x.getEndTime()+"\n"+ x.getActivityLabel());
			}
		}
	   
		
		this.setDate(data);
		
	}

	
	public int countdays(){
		int count=1;
	
		for(int i=0;i<this.getDate().size();i++)
		{
			if(this.getDate().get(i).getStartTime().getDay()!=this.getDate().get(i).getEndTime().getDay())
				count++;
		}
			
		
		System.out.println("Sunt "+ count+" zile diferite.\n");
		return count;
	}
		
	public void createMap(){
		
		for(int i=0;i<this.getDate().size();i++)
		{
			String cAction=this.getDate().get(i).getActivityLabel();
			//System.out.println(cAction);
			if(!actionMap.containsKey(cAction))
			{
				actionMap.put(cAction, null);
				System.out.println("Activity "+ cAction+" was added.");
			}
			
		}
		
		for(Entry<String,Integer> a:actionMap.entrySet()){
			Integer countAction=0;
			for(int i=0;i<this.getDate().size();i++)
			{
				if(this.getDate().get(i).getActivityLabel().equals(a.getKey()))
					countAction++;
					
			}
			this.actionMap.put(a.getKey(), countAction);
			System.out.println("Entry with key  "+ a.getKey()+" and count "+countAction +" was added in map.");
		}
	}
	
	public void filter(){
		for(Entry<String,Integer> a:actionMap.entrySet()){
			String activity=a.getKey();
			int gasire=0;
			for(MonitoredData m:this.date){
				if(activity.equals(m.getActivityLabel())){
					//se aduna minutele daca sunt mai mici ca 5;
					if(m.getEndTime().getHour()==m.getStartTime().getHour())	
					{
						if(m.getEndTime().getMinute()>=m.getStartTime().getMinute()){
							if((m.getEndTime().getMinute()*60+m.getEndTime().getSecond())-(m.getStartTime().getMinute()*60+m.getStartTime().getSecond())<=300)
								gasire++;
						}
						else
							if(m.getEndTime().getMinute()<m.getStartTime().getMinute()){
								if(((60-m.getEndTime().getMinute())*60+m.getEndTime().getSecond())-(m.getStartTime().getMinute()*60+m.getStartTime().getSecond())<=300){
									gasire++;
								}
							}
					}
				}
			}
				
			if(gasire>=(a.getValue()*0.9)){
				this.filteredActivities.add(activity);
				System.out.println(activity+ " a fost adaugat la lista filtrata");
			
			}
	}}

	public List<String> getFilteredActivities() {
		return filteredActivities;
	}

	public void setFilteredActivities(List<String> filteredActivities) {
		this.filteredActivities = filteredActivities;
	}
	public void writeListinFile(){
		try {
			PrintWriter out = new PrintWriter("C:/Users/Alex/Desktop/filteredlist.txt");
			for(int i=0;i<this.filteredActivities.size();i++)
			out.println(this.filteredActivities.get(i));
			out.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}
	public void writeMapinFile(){
		try {
			PrintWriter out = new PrintWriter("C:/Users/Alex/Desktop/map.txt");
			for(Entry<String,Integer> a:actionMap.entrySet())
				out.println(a.getKey()+" "+a.getValue());
			out.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}
	public static void main(String args[]) {

		Monitoring t= new Monitoring();
		try {
			List<String> lista= t.readFile();
			t.alldata(lista);
			t.countdays();
			t.createMap();
			t.filter();
			t.writeListinFile();
			t.writeMapinFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	

	}


	
}
